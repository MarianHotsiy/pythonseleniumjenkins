import pytest
from pytest import mark


@mark.smoke
def test_always_true():
    assert True


@mark.smoke
@mark.skip(reason='because it always false')
def test_always_false():
    assert False
